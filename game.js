'use strict';

class Vector {
    constructor(x = 0, y = 0) {
        this.x = x;
        this.y = y;
    }
    plus(vector) {
        if (!(vector instanceof Vector)) {
            throw new Error('Объект должен быть типа Vector');
        }
        return new Vector(this.x + vector.x, this.y + vector.y);
    }
    times(number) {
        return new Vector(this.x * number, this.y * number);
    }
}

class Actor {
    constructor(pos = new Vector(0, 0), size = new Vector(1, 1), speed = new Vector(0, 0)) {
        if (pos instanceof Vector && size instanceof Vector && speed instanceof Vector) {
            this.pos = pos;
            this.size = size;
            this.speed = speed;
        } else {
            throw new Error('Объект не является типом Vector');
        }
    }
    act() {}
    get left() {
        return this.pos.x;
    }
    get top() {
        return this.pos.y;
    }
    get bottom() {
        return this.pos.y + this.size.y;
    }
    get right() {
        return this.pos.x + this.size.x;
    }
    get type() {
        return 'actor';
    }
    isIntersect(actor) {
        if (!(actor instanceof Actor)) {
            throw new Error('Объект должен быть типа Actor');
        } else if (actor === undefined) {
            throw new Error('Объект не должен быть пустым');
        }
        if (actor === this) {
            return false;
        }
        if (actor.left >= this.right || actor.right <= this.left || actor.top >= this.bottom || actor.bottom <= this.top) {
            return false;
        }
        return true;
    }
}

class Level {
    constructor(grid = [], actors = []) {
        this.grid = grid;
        this.actors = actors;
        this.player = actors.find(el => el.type === 'player');
        this.height = grid.length;
        this.width = Math.max(0, ...(this.grid.map(el => el.length)));
        this.status = null;
        this.finishDelay = 1;
    }
    isFinished() {
        return this.status !== 0 && this.finishDelay < 0;
    }
    actorAt(actor) {
        if (!(actor instanceof Actor)) {
            throw new Error('Объект должен быть типа Actor');
        } else if (actor === undefined) {
            throw new Error('Объект не должен быть пустым');
        }
        if (this.actors === undefined || this.actors.length === 1) {
            return undefined;
        } else if (this.actors.find((el) => el.isIntersect(actor))) {
            return this.actors.find((el) => el.isIntersect(actor));
        } 
    }
    obstacleAt(pos, size) {
        if (!(pos instanceof Vector) || !(size instanceof Vector)) {
            throw new Error('Объект должен быть типа Vector');
        }
        const left = Math.floor(pos.x);
        const right = Math.ceil(pos.x + size.x);
        const top = Math.floor(pos.y);
        const bottom = Math.ceil(pos.y + size.y);
        if (left < 0 || right > this.width || top < 0) {
            return 'wall';
        }
        if (bottom > this.height) {
            return 'lava';
        }
        for (let y = top; y < bottom; y++) {
            for (let x = left; x < right; x++) {
                const gridLevel = this.grid[y][x];
                if (gridLevel) {
                    return gridLevel;
                }
            }
        }
    }
    removeActor(actor) {
        const index = this.actors.indexOf(actor);
        if (index !== -1) {
            this.actors.splice(index, 1);
        }
    }
    noMoreActors(type) {
        return !this.actors.some((actor) => actor.type === type)
    }
    playerTouched(touch, actor) {
        if (this.status !== null) {
            return;
        }
        if (['lava', 'fireball'].some((el) => el === touch)) {
            return this.status = 'lost';
        }
        if (touch === 'coin' && actor.type === 'coin') {
            this.removeActor(actor);
            if (this.noMoreActors('coin')) {
                return this.status = 'won'
            }
        }
    }
}


class LevelParser {
    constructor(letters = {}) {
        this.letters = Object.assign({}, letters);
    }
    actorFromSymbol(letter) {
        return this.letters[letter];
    }
    obstacleFromSymbol(letter) {
        if (letter === 'x') {
            return 'wall'
        }
        if (letter === '!') {
            return 'lava'
        }
    }
    createGrid(plan) {
        return plan.map(line => line.split('')).map(line => line.map(line => this.obstacleFromSymbol(line)));
    }
    createActors(plan) {
        return plan.reduce((rez, itemY, y) => {
            itemY.split('').forEach((itemX, x) => {
                const constructor = this.actorFromSymbol(itemX);
                if (typeof constructor === 'function') {
                    const actor = new constructor(new Vector(x, y));
                    if (actor instanceof Actor) {
                        rez.push(actor);
                    }
                }
            });
            return rez;
        }, []);
    }
    parse(plan) {
        return new Level(this.createGrid(plan), this.createActors(plan));
    }
}

class Fireball extends Actor {
    constructor(pos = new Vector(0, 0), speed = new Vector(0, 0)) {
        super(pos, new Vector(1, 1), speed);
    }
    get type() {
        return 'fireball';
    }
    getNextPosition(time = 1) {
        return this.pos.plus(this.speed.times(time));
    }
    handleObstacle() {
        this.speed = this.speed.times(-1);
    }
    act(time, level) {
        const next = this.getNextPosition(time);
        if (level.obstacleAt(next, this.size)) {
            this.handleObstacle();
        } else {
            this.pos = next
        }
    }
}

class HorizontalFireball extends Fireball {
    constructor(pos = new Vector(0, 0)) {
        super(pos, new Vector(2, 0));
    }
}

class VerticalFireball extends Fireball {
    constructor(pos = new Vector(0, 0)) {
        super(pos, new Vector(0, 2));
    }
}

class FireRain extends Fireball {
    constructor(pos = new Vector(0, 0)) {
        super(pos, new Vector(0, 3));
        this.startPos = this.pos;
    }

    handleObstacle() {
        this.pos = this.startPos;
    }
}

class Coin extends Actor {
    constructor(pos = new Vector(0, 0)) {
        super(pos.plus(new Vector(0.2, 0.1)), new Vector(0.6, 0.6));
        this.spring = Math.random() * (Math.PI * 2);
        this.springSpeed = 8;
        this.springDist = 0.07;
        this.startPos = this.pos;
    }

    get type() {
        return 'coin';
    }

    updateSpring(time = 1) {
        this.spring += this.springSpeed * time;
    }

    getSpringVector() {
        return new Vector(0, Math.sin(this.spring) * this.springDist)
    }

    getNextPosition(time = 1) {
        this.updateSpring(time);
        return this.startPos.plus(this.getSpringVector());
    }

    act(time) {
        this.pos = this.getNextPosition(time);
    }
}

class Player extends Actor {
    constructor(pos = new Vector(0, 0)) {
        super(pos.plus(new Vector(0, -0.5)), new Vector(0.8, 1.5));
    }

    get type() {
        return 'player';
    }
}

const actorDict = {
    '@': Player,
    'v': FireRain,
    'o': Coin,
    '=': HorizontalFireball,
    '|': VerticalFireball

};
const parser = new LevelParser(actorDict);

loadLevels()
    .then((res) => {
        runGame(JSON.parse(res), parser, DOMDisplay)
            .then(() => alert('Вы выиграли!'))
    });

const grid = [
    new Array(3), ['wall', 'wall', 'lava']
];
const level = new Level(grid);
runLevel(level, DOMDisplay);